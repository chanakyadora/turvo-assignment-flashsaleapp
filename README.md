**FLASH SALE APP DEMO**

**Projects Specs :**

* **Platform :** Java8
* **Framework :** SpringBoot, Hibernate
* **Database:** MySQL

****Business Logic :****

* Only registered user can place order during a flash sale.
* Registration to a sale is not allowed when the sale is active.
* Register for the sale before it starts -> Start the sale -> Place order for the product on offer.


**APIs Exposed:**

**1. Adding new Product:**


* **HTTP Method:** POST  
* **Request:** http://localhost:8084/turvo/addProduct/
* **Message:** Product added successfully
* **Description:** Adds a new product according to the user input.


**2. Flash Sale:**

* **HTTP Method:** PUT
* **Request:** 1. http://localhost:8084/sale/startSale/?saleId=SALE1 
               2. http://localhost:8084/sale/endSale/?saleId=SALE2
* **Message:** Flash Sale Started/Flash Sale Ended
* ****Description:**** Starts a flash sale/Ends a flash sale.


**3. Customer Registration:**

* **HTTP Method:** POST
* **Request:** http://localhost:8084/register/registerForSale/
* **Message:** Registered Successfully for the sale
* **Description:** Allows a user to register for sale and customer status will get updated to registered once registration is done.


**4. Product Purchase:**

* **HTTP Method:** POST
* **Request:** http://localhost:8084/order/orderProduct/
* **Message:** Your Order is processed successfully
* **Description:** Allows a registered user to place order. Before purchase it will check whether user is regestered or not.

**ER Diagram:**

![ER_Diagram](/uploads/9673ebfced52605fa2237f7394508017/ER_Diagram.png)





