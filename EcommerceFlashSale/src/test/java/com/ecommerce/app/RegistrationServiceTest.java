package com.ecommerce.app;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.ecommerce.app.dao.FlashSaleDAO;
import com.ecommerce.app.dao.RegistrationDAO;
import com.ecommerce.app.entities.EcommerceFlashSale;
import com.ecommerce.app.entities.UserRegistration;
import com.ecommerce.app.service.RegistrationService;


@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceTest {

	@InjectMocks
	private RegistrationService registrationService;

	@Mock
	RegistrationDAO regDataService;

	@Mock
	FlashSaleDAO flashSaleDataService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRegisterForSale() {
		UserRegistration registration = new UserRegistration();
		when(regDataService.registerForSale(registration)).thenReturn(2);
		Assert.assertEquals( (regDataService.registerForSale(registration)), 2);
	}

	@Test
	public void testCheckRegistrationEligibility() {

		EcommerceFlashSale sale = new EcommerceFlashSale();
		sale.setRegistrationOpen(Boolean.TRUE);
		when(flashSaleDataService.findBySaleId("sale1")).thenReturn(sale);

		List<UserRegistration> reg = new ArrayList<>();
		when(regDataService.findByCustomerIdAndSaleId("14270", "SALE1")).thenReturn(reg);

		Assert.assertEquals(flashSaleDataService.findBySaleId("sale1").getRegistrationOpen()
				&& regDataService.findByCustomerIdAndSaleId("14270", "SALE1").isEmpty(), Boolean.TRUE);

	}

}
