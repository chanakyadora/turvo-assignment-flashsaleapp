package com.ecommerce.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.ProductInventory;

@Repository
public interface ProductInventoryRepository extends JpaRepository<ProductInventory, String>{
	
}
