package com.ecommerce.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.PurchaseProduct;


@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseProduct, Double>{

}
