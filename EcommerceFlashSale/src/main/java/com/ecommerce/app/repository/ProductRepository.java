package com.ecommerce.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.Products;


@Repository
public interface ProductRepository extends JpaRepository<Products, Double>{

	
}
