package com.ecommerce.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.EcommerceFlashSale;


@Repository
public interface FlashSaleRepository extends JpaRepository<EcommerceFlashSale, String>{
	
	Optional<EcommerceFlashSale> findById(String saleId);
}
