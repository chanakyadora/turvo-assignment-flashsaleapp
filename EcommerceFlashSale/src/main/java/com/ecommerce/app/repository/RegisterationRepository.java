package com.ecommerce.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.UserRegistration;

@Repository
public interface RegisterationRepository extends JpaRepository<UserRegistration, Double>{

	
}
