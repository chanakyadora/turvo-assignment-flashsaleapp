package com.ecommerce.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ecommerce.app.entities.Customer;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, String>{
	
}
