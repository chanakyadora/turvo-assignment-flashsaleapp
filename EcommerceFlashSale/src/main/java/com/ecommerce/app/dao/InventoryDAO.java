package com.ecommerce.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Service;

import com.ecommerce.app.entities.ProductInventory;

@Service
public class InventoryDAO implements IInventoryDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Double returnProductInventory(Double productId) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductInventory> cq = cb.createQuery(ProductInventory.class);
 
        Root<ProductInventory> root = cq.from(ProductInventory.class);
        Predicate prodId = cb.equal(root.get("productId"), productId);
        cq.where(prodId);
        
        TypedQuery<ProductInventory> query = entityManager.createQuery(cq);
        ProductInventory inventory = query.getSingleResult();
        
		return inventory.getQuantity();
	}


}
