package com.ecommerce.app.dao;

import com.ecommerce.app.entities.PurchaseProduct;

public interface IPurchaseOrderDAO {
	
	public PurchaseProduct saveOrder(PurchaseProduct order);

}
