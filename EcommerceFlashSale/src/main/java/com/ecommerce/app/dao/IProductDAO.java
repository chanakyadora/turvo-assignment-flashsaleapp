package com.ecommerce.app.dao;

import com.ecommerce.app.entities.Products;

public interface IProductDAO {
	
	public void saveProduct(Products product);

}
