package com.ecommerce.app.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ecommerce.app.entities.EcommerceFlashSale;
import com.ecommerce.app.repository.FlashSaleRepository;
import com.ecommerce.app.utilities.FlashSaleStatus;


@Service
public class FlashSaleDAO implements IFlashSaleDAO {
	
	@Autowired
	FlashSaleRepository repository;

	@Override
	public void startFlashSale(String flashSaleId) {
		Optional<EcommerceFlashSale> sale = repository.findById(flashSaleId);
		EcommerceFlashSale flashSale = sale.get();
		flashSale.setStatus(FlashSaleStatus.ACTIVE);
		flashSale.setRegistrationOpen(Boolean.FALSE);
		repository.save(flashSale);
	}
	
	@Override
	public void endFlashSale(String flashSaleId) {
		Optional<EcommerceFlashSale> sale = repository.findById(flashSaleId);
		EcommerceFlashSale flashSale = sale.get();
		flashSale.setStatus(FlashSaleStatus.ENDED);
		flashSale.setRegistrationOpen(Boolean.TRUE);
		repository.save(flashSale);
	}

	@Override
	public EcommerceFlashSale findBySaleId(String saleId) {
		return repository.findById(saleId).get();
	}

}
