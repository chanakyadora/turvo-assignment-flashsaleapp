package com.ecommerce.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.entities.Customer;
import com.ecommerce.app.entities.UserRegistration;
import com.ecommerce.app.repository.CustomerRepository;
import com.ecommerce.app.repository.RegisterationRepository;
import com.ecommerce.app.utilities.CustomerRegistrationStatus;
import com.ecommerce.app.utilities.CustomerStatus;


@Service
public class RegistrationDAO implements IRegistrationDAO {

	@Autowired
	RegisterationRepository repository;
	
	@Autowired
	CustomerRepository custrepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public int registerForSale(UserRegistration registration) {
		UserRegistration entry = repository.save(registration);
		updateCustomerStatus(registration.getCustomerId(),CustomerStatus.REGESTERED);
		return entry.getRegistrationId();
	}

	@Override
	public List<UserRegistration> findByCustomerIdAndSaleId(String customerId, String saleId) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserRegistration> cq = cb.createQuery(UserRegistration.class);
 
        Root<UserRegistration> root = cq.from(UserRegistration.class);
        Predicate CustomerId = cb.equal(root.get("customerId"), customerId);
        Predicate saleid = cb.equal(root.get("flashSaleId"), saleId);
        Predicate status = cb.equal(root.get("regStatus"), CustomerRegistrationStatus.REGISTERED);
        cq.where(CustomerId, saleid,status);
        
        TypedQuery<UserRegistration> query = entityManager.createQuery(cq);
        List<UserRegistration> reg =  query.getResultList();
		return reg;
	}

	@Override
	public void updateRegistrationStatus(String customerId, String saleId, CustomerRegistrationStatus status) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserRegistration> cq = cb.createQuery(UserRegistration.class);
 
        Root<UserRegistration> root = cq.from(UserRegistration.class);
        Predicate CustomerId = cb.equal(root.get("customerId"), customerId);
        Predicate saleid = cb.equal(root.get("flashSaleId"), saleId);
        Predicate status1 = cb.equal(root.get("regStatus"), CustomerRegistrationStatus.REGISTERED);
        cq.where(CustomerId, saleid,status1);
        
        TypedQuery<UserRegistration> query = entityManager.createQuery(cq);
        List<UserRegistration> registration =  query.getResultList();
        for(UserRegistration reg:registration)
        {
        	reg.setRegStatus(status);
        	repository.save(reg);
        	
        }
	}
	
public void updateCustomerStatus(String customerId,CustomerStatus custstatus) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
 
        Root<Customer> root = cq.from(Customer.class);
        Predicate CustomerId = cb.equal(root.get("customerId"), customerId);
        cq.where(CustomerId);
        
        TypedQuery<Customer> query = entityManager.createQuery(cq);
        Customer customer =  query.getSingleResult();
        
        if(customer!=null)
        {
        	customer.setStatus(custstatus);
            custrepository.save(customer);
        }
        
	}
}
