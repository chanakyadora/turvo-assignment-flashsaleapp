package com.ecommerce.app.dao;

public interface IInventoryDAO {
	
	public Double returnProductInventory(Double productId);
	
}
