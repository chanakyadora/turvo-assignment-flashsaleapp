package com.ecommerce.app.dao;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.entities.Customer;
import com.ecommerce.app.entities.PurchaseProduct;
import com.ecommerce.app.repository.CustomerRepository;
import com.ecommerce.app.repository.PurchaseOrderRepository;
import com.ecommerce.app.utilities.CustomerStatus;


@Service
public class PurchaseOrderDAO implements IPurchaseOrderDAO {
	
	@Autowired
	PurchaseOrderRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	CustomerRepository custrepository;

	@Override
	public PurchaseProduct saveOrder(PurchaseProduct order) {
		Optional<String> checkNull = Optional.ofNullable(order.getCustomerId());
		if(checkNull.isPresent())
		{
			updateCustomerStatus(order.getCustomerId(),CustomerStatus.PURCHASED);
		}
		return repository.save(order);
	}
	
public void updateCustomerStatus(String customerId,CustomerStatus custstatus) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> cq = cb.createQuery(Customer.class);
 
        Root<Customer> root = cq.from(Customer.class);
        Predicate CustomerId = cb.equal(root.get("customerId"), customerId);
        cq.where(CustomerId);
        
        TypedQuery<Customer> query = entityManager.createQuery(cq);
        Customer customer =  query.getSingleResult();
        
        if(customer!=null)
        {
        	customer.setStatus(custstatus);
            custrepository.save(customer);
        }
        
	}

}
