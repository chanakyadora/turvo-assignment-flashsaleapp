package com.ecommerce.app.dao;

import java.util.List;
import com.ecommerce.app.entities.UserRegistration;
import com.ecommerce.app.utilities.CustomerRegistrationStatus;


public interface IRegistrationDAO {
	
	public int registerForSale(UserRegistration registration);
	
	public  List<UserRegistration> findByCustomerIdAndSaleId(String customerId, String saleId);
	
	public void updateRegistrationStatus(String customerId, String saleId, CustomerRegistrationStatus status);

}
