package com.ecommerce.app.dao;

import com.ecommerce.app.entities.EcommerceFlashSale;

public interface IFlashSaleDAO {
	
	public void startFlashSale(String flashSaleId);
	
	public void endFlashSale(String flashSaleId);
	
	public EcommerceFlashSale findBySaleId(String saleId);

}
