package com.ecommerce.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ecommerce.app.entities.Products;
import com.ecommerce.app.repository.ProductRepository;


@Service
public class ProductDAO implements IProductDAO {
	
	@Autowired
	ProductRepository repository;
	
	@Override
	public void saveProduct(Products product) {
		
		repository.save(product);
		
	}
	
}
