package com.ecommerce.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceFlashSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceFlashSaleApplication.class, args);
	}

}
