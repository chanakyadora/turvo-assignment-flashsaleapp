package com.ecommerce.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.dao.IFlashSaleDAO;
import com.ecommerce.app.dao.IRegistrationDAO;
import com.ecommerce.app.entities.EcommerceFlashSale;
import com.ecommerce.app.entities.UserRegistration;
import com.ecommerce.app.pojo.RegistrationRequest;
import com.ecommerce.app.utilities.CustomerRegistrationStatus;


@Service
public class RegistrationService implements IUserRegistrationService {
	
	@Autowired
	IRegistrationDAO regDataService;
	
	@Autowired
	IFlashSaleDAO flashSaleDataService;

	@Override
	public int registerForSale(RegistrationRequest request) {
		int regId = 0;
		if(checkRegistrationEligibility(request)) {
			UserRegistration registration = registrationDAOConvertor(request);
			regId = regDataService.registerForSale(registration);
		}
		return regId;
	}
	
	private UserRegistration registrationDAOConvertor(RegistrationRequest request) {
		UserRegistration registration = new UserRegistration();
		registration.setCustomerId(request.getCustomerId());
		registration.setFlashSaleId(request.getSaleId());
		registration.setRegStatus(CustomerRegistrationStatus.REGISTERED);
		return registration;
	}
	
	private Boolean checkRegistrationEligibility(RegistrationRequest request) {
		
		Boolean registrationOpen = Boolean.FALSE;
		Boolean isEligible = Boolean.FALSE;
		EcommerceFlashSale sale = flashSaleDataService.findBySaleId(request.getSaleId());
		registrationOpen = sale.getRegistrationOpen();
		List<UserRegistration> reg = regDataService.findByCustomerIdAndSaleId(request.getCustomerId(), request.getSaleId());
		if(reg.isEmpty()) isEligible = Boolean.TRUE;
		
		return registrationOpen && isEligible;
	}
	
}