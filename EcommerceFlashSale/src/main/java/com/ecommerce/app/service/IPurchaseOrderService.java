package com.ecommerce.app.service;

import com.ecommerce.app.pojo.OrderResponse;
import com.ecommerce.app.pojo.PurchaseOrderRequest;

public interface IPurchaseOrderService {
	
	public OrderResponse placeOrder(PurchaseOrderRequest orderRequest);

}
