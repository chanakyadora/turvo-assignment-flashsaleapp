package com.ecommerce.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.dao.IFlashSaleDAO;

@Service
public class FlashSaleService implements IFlashSaleService {

	@Autowired
	IFlashSaleDAO saleDataService;
	
	@Override
	public void startSale(String saleId) {
		saleDataService.startFlashSale(saleId);
		
	}

	@Override
	public void endSale(String saleId) {
		
		saleDataService.endFlashSale(saleId);
		
	}

}
