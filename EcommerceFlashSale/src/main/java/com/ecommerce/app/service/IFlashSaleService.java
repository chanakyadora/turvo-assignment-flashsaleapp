package com.ecommerce.app.service;

public interface IFlashSaleService {
	
	public void startSale(String saleId);
	
	public void endSale(String saleId);

}
