package com.ecommerce.app.service;

import com.ecommerce.app.pojo.RegistrationRequest;

public interface IUserRegistrationService {
	
	public int registerForSale (RegistrationRequest request);

}
