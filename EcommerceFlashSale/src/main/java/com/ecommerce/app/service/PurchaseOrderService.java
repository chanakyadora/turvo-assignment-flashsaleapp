package com.ecommerce.app.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.app.dao.IFlashSaleDAO;
import com.ecommerce.app.dao.IInventoryDAO;
import com.ecommerce.app.dao.IPurchaseOrderDAO;
import com.ecommerce.app.dao.IRegistrationDAO;
import com.ecommerce.app.entities.EcommerceFlashSale;
import com.ecommerce.app.entities.PurchaseProduct;
import com.ecommerce.app.entities.UserRegistration;
import com.ecommerce.app.pojo.OrderResponse;
import com.ecommerce.app.pojo.PurchaseOrderRequest;
import com.ecommerce.app.utilities.AppConstants;
import com.ecommerce.app.utilities.CustomerRegistrationStatus;
import com.ecommerce.app.utilities.FlashSaleStatus;
import com.ecommerce.app.utilities.ProductStatus;

@Service
public class PurchaseOrderService implements IPurchaseOrderService {
	
	@Autowired
	IInventoryDAO inventoryDataService;
	
	@Autowired
	IRegistrationDAO regDataService;
	
	@Autowired
	IPurchaseOrderDAO orderDataService;
	
	@Autowired
	IFlashSaleDAO saleDataService;

	@Override
	public OrderResponse placeOrder(PurchaseOrderRequest orderRequest) {
		OrderResponse response = new OrderResponse();
		if (checkSaleActive(orderRequest.getSaleId()) && checkInventory(orderRequest.getProductId())
				&& checkRegistration(orderRequest.getCustomerId(), orderRequest.getSaleId())) {
			System.out.println("chanakya inside if check");
			PurchaseProduct order = saveOrder(orderRequest);
			updateRegistrationStatus(orderRequest.getCustomerId(), orderRequest.getSaleId());
			response.setMessage(AppConstants.ORDER_SUCCESS_MESSAGE);
			response.setOrderId(order.getOrderId());
			response.setProductId(order.getProductId());
			response.setStatus(order.getOrderStatus().toString());
		} else {
			response.setMessage(AppConstants.ORDER_ERROR_MESSAGE);
		}
		return response;
	}
	
	private Boolean checkInventory(Double productId) {
		Double quantity = 0d;
		quantity = inventoryDataService.returnProductInventory(productId);
		return quantity > 0d;
	}
	
	private Boolean checkSaleActive(String saleId) {
		EcommerceFlashSale sale = saleDataService.findBySaleId(saleId);
		if(sale.getStatus().equals(FlashSaleStatus.ACTIVE)) {
			return Boolean.TRUE;
		} 
		return Boolean.FALSE;
	}
	
	private Boolean checkRegistration(String customerId, String saleId) {
		List<UserRegistration> registration  = regDataService.findByCustomerIdAndSaleId(customerId, saleId);
		return registration.size()==1;
	}
	
	private void updateRegistrationStatus(String customerId, String saleId) {
		CustomerRegistrationStatus status = CustomerRegistrationStatus.USED;
		regDataService.updateRegistrationStatus(customerId, saleId, status);
	}
	
	private PurchaseProduct saveOrder(PurchaseOrderRequest orderRequest) {
		PurchaseProduct order = purchaseOrderDAOConvertor(orderRequest);
		return orderDataService.saveOrder(order);
	}
	
	private PurchaseProduct purchaseOrderDAOConvertor(PurchaseOrderRequest orderRequest) {
		PurchaseProduct order = new PurchaseProduct();
		order.setCustomerId(orderRequest.getCustomerId());
		order.setLastUpdated(new Date());
		order.setOrderedAt(new Date());
		order.setProductId(orderRequest.getProductId());
		order.setOrderStatus(ProductStatus.PROCESSED);
		return order;
	}
}
