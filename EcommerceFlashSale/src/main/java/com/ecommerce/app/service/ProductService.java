package com.ecommerce.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.ecommerce.app.daoo.ICountersDataService;
import com.ecommerce.app.dao.IProductDAO;
import com.ecommerce.app.entities.Products;
import com.ecommerce.app.pojo.TurvoProduct;


@Service
public class ProductService implements IProductService {
	
	@Autowired
	IProductDAO productDataService;
	
	@Override
	public void saveNewProduct(TurvoProduct product) {
		Products newProduct = productDAOConvertor(product);
		productDataService.saveProduct(newProduct);
	}
	
	private Products productDAOConvertor (TurvoProduct product) {
		Products dbProductEntity = new Products();
		dbProductEntity.setDescription(product.getDescription());
		dbProductEntity.setName(product.getName());
		dbProductEntity.setPrice(product.getPrice());
		return dbProductEntity;
	}

}