package com.ecommerce.app.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ecommerce.app.utilities.FlashSaleStatus;

@Entity
@Table(name = "EcommerceFlashSale")
public class EcommerceFlashSale {
	
	@Id
	private String flashSaleId;
	
	@Column(name = "ProductId")
	private int productId;
	
	@Column(name = "Status")
	private FlashSaleStatus status;
	
	@Column(name = "Duration")
	private String durationInSeconds;
	
	@Column(name = "RegistrationOpen")
	private Boolean registrationOpen;

	public String getFlashSaleId() {
		return flashSaleId;
	}

	public void setFlashSaleId(String flashSaleId) {
		this.flashSaleId = flashSaleId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public FlashSaleStatus getStatus() {
		return status;
	}

	public void setStatus(FlashSaleStatus status) {
		this.status = status;
	}

	public String getDurationInSeconds() {
		return durationInSeconds;
	}

	public void setDurationInSeconds(String durationInSeconds) {
		this.durationInSeconds = durationInSeconds;
	}

	public Boolean getRegistrationOpen() {
		return registrationOpen;
	}

	public void setRegistrationOpen(Boolean registrationOpen) {
		this.registrationOpen = registrationOpen;
	}

}