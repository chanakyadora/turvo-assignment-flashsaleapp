package com.ecommerce.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ecommerce.app.utilities.CustomerRegistrationStatus;

@Entity
@Table(name = "CustomerRegistration")
public class UserRegistration {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int registrationId;
	
	@Column(name = "CustomerId")
	private String customerId;
	
	@Column(name = "FlashSaleId")
	private String flashSaleId;
	
	@Column(name = "Status")
	private CustomerRegistrationStatus regStatus;

	public int getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(int registrationId) {
		this.registrationId = registrationId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getFlashSaleId() {
		return flashSaleId;
	}

	public void setFlashSaleId(String flashSaleId) {
		this.flashSaleId = flashSaleId;
	}

	public CustomerRegistrationStatus getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(CustomerRegistrationStatus regStatus) {
		this.regStatus = regStatus;
	}

}
