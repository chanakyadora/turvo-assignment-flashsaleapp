package com.ecommerce.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.ecommerce.app.utilities.CustomerStatus;


@Entity
@Table(name = "Customer")
public class Customer {
	
	@Id
	private String customerId;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name= "Address")
	private String shipToAddress;
	
	@Column(name = "Status")
	private CustomerStatus status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getShipToAddress() {
		return shipToAddress;
	}

	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}

	public CustomerStatus getStatus() {
		return status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

}
