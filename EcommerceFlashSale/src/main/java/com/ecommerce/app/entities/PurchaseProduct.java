package com.ecommerce.app.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ecommerce.app.utilities.ProductStatus;


@Entity
@Table(name = "PurchaseProduct")
public class PurchaseProduct {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int orderId;
	
	@Column(name = "ProductId")
	private Double productId;
	
	@Column(name = "CustomerId")
	private String customerId;
	
	@Column(name = "CreatedAt")
	private Date orderedAt;
	
	@Column(name = "UpdatedAt")
	private Date lastUpdated;
	
	@Column(name = "Status")
	private ProductStatus orderStatus;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Double getProductId() {
		return productId;
	}

	public void setProductId(Double productId) {
		this.productId = productId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getOrderedAt() {
		return orderedAt;
	}

	public void setOrderedAt(Date orderedAt) {
		this.orderedAt = orderedAt;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public ProductStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(ProductStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

}
