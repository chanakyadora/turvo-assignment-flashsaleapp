package com.ecommerce.app.utilities;

public enum ProductStatus {
	
	PROCESSED,
	CANCELLED,
	DRAFTED,
	DISPATCHED,
	DELIVERED

}
