package com.ecommerce.app.utilities;

public enum FlashSaleStatus {
	
	ACTIVE,
	INACTIVE,
	ENDED,
	EXPIRED

}
