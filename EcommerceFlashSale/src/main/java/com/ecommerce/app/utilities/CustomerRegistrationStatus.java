package com.ecommerce.app.utilities;

public enum CustomerRegistrationStatus {
	
	REGISTERED,
	USED,
	CANCELLED

}
