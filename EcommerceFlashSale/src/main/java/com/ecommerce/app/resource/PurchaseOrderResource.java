package com.ecommerce.app.resource;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ecommerce.app.pojo.OrderResponse;
import com.ecommerce.app.pojo.PurchaseOrderRequest;
import com.ecommerce.app.service.IPurchaseOrderService;
import com.ecommerce.app.utilities.AppConstants;
import com.ecommerce.app.utilities.Response;

@RestController
@RequestMapping("/order")
@CrossOrigin
public class PurchaseOrderResource {
	
	@Autowired
	IPurchaseOrderService orderService;
	
	@PostMapping(value = "/orderProduct", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> orderProduct(@RequestBody PurchaseOrderRequest orderRequest, HttpServletRequest request) {
		
		OrderResponse response = orderService.placeOrder(orderRequest);
		
		if(response.getMessage().equals(AppConstants.ORDER_SUCCESS_MESSAGE)) {
			return new ResponseEntity<Response>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<Response>(response, HttpStatus.BAD_REQUEST);
		}
	}

}
