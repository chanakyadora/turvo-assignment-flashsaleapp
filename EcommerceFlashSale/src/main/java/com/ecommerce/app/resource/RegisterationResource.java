package com.ecommerce.app.resource;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.app.pojo.RegistrationRequest;
import com.ecommerce.app.pojo.RegistrationResponse;
import com.ecommerce.app.service.IUserRegistrationService;
import com.ecommerce.app.utilities.AppConstants;
import com.ecommerce.app.utilities.Response;


@RestController
@RequestMapping("/register")
@CrossOrigin
public class RegisterationResource {
	
	@Autowired
	IUserRegistrationService regService;
	
	@PostMapping(value = "/registerForSale", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> register(@RequestBody RegistrationRequest regRequest, HttpServletRequest httpRequest) {
		int regId = 0;
		HttpStatus status;
		regId = regService.registerForSale(regRequest);
		RegistrationResponse response = new RegistrationResponse();
		if (regId!=0) {			
			response.setMessage(AppConstants.REGISTRATION_SUCCESS_MESSAGE);
			response.setRegistrationId(regId);
			status = HttpStatus.OK;
		} else {
			response.setMessage(AppConstants.REGISTRATION_ERROR_MESSAGE);
			response.setRegistrationId(regId);
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<Response>(response, status);
	}

}
