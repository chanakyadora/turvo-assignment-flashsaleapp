package com.ecommerce.app.resource;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.app.pojo.ProductResponse;
import com.ecommerce.app.pojo.TurvoProduct;
import com.ecommerce.app.service.IProductService;
import com.ecommerce.app.utilities.AppConstants;
import com.ecommerce.app.utilities.Response;


@RestController
@CrossOrigin
@RequestMapping("/turvo")
public class ProductResource {
	
	@Autowired
	IProductService productService;
	
	@PostMapping(value = "/addProduct", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> addNewProduct(@RequestBody TurvoProduct product, HttpServletRequest request) {
		
		productService.saveNewProduct(product);
		
		ProductResponse response = new ProductResponse();
		response.setMessage(AppConstants.PRODUCT_ADD_SUCCESS_MESSAGE);
		
		return new ResponseEntity<Response>(response, HttpStatus.OK);
		
	}

}
