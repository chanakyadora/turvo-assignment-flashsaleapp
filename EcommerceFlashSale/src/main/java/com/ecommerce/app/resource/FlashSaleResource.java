package com.ecommerce.app.resource;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ecommerce.app.pojo.FlashSaleResponse;
import com.ecommerce.app.service.IFlashSaleService;
import com.ecommerce.app.utilities.AppConstants;
import com.ecommerce.app.utilities.Response;

@RestController
@RequestMapping("/sale")
@CrossOrigin
public class FlashSaleResource {
	
	@Autowired
	IFlashSaleService saleService;
	
	@PutMapping(value = "/startSale", produces = "application/json")
	public ResponseEntity<Response> startSale(@RequestParam String saleId, HttpServletRequest request) {
		saleService.startSale(saleId);
		
		FlashSaleResponse response = new FlashSaleResponse();
		response.setMessage(AppConstants.SALE_START_MESSAGE);
		
		return new ResponseEntity<Response>(response, HttpStatus.OK);
		
	}
	
	@PutMapping(value = "/endSale", produces = "application/json")
	public ResponseEntity<Response> endSale(@RequestParam String saleId, HttpServletRequest request) {
		
		saleService.endSale(saleId);
		
		FlashSaleResponse response = new FlashSaleResponse();
		response.setMessage(AppConstants.SALE_STOP_MESSAGE);
		
		return new ResponseEntity<Response>(response, HttpStatus.OK);
		
	}


}
