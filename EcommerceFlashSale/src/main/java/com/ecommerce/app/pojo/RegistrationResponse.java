package com.ecommerce.app.pojo;

import com.ecommerce.app.utilities.Response;

public class RegistrationResponse extends Response {

	private static final long serialVersionUID = 1L;
	
	private int registrationId;

	public int getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(int regId) {
		this.registrationId = regId;
	}

}
