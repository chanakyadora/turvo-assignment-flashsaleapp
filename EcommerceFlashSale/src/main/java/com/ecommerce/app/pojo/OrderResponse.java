package com.ecommerce.app.pojo;

import com.ecommerce.app.utilities.Response;

public class OrderResponse extends Response {

	private static final long serialVersionUID = 1L;
	
	private Double productId;
		
	private int orderId;
	
	private String status;

	public Double getProductId() {
		return productId;
	}

	public void setProductId(Double productId) {
		this.productId = productId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int i) {
		this.orderId = i;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
